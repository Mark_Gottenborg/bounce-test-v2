import React from 'react'
import Product from './partials/product'
import PersonalInformation from './partials/personalInformation'
import PaymentDetails from './partials/paymentDetails'
import CartTotalSection from './partials/cartTotalSection'
import { connect } from 'react-redux'


const Content = ({progressIndex}) => {
  console.log('progressIndex', progressIndex)
  switch (progressIndex) {
    case 0:
      return (
        <div>
          <Product/>
          <CartTotalSection/>
        </div>
      )
      break;

    case 1:
      return (
        <div>
          <Product/>
          <PersonalInformation/>
          <CartTotalSection/>
        </div>
      )
      break;

    case 2:
      return (
        <div>
          <Product/>
          <PersonalInformation/>
          <PaymentDetails/>
          <CartTotalSection/>
        </div>
      )
      break;

    case 3:
      return (
        <div className="container-fluid d-flex justify-content-center">
          <h1>Your Booking has been made</h1>
        </div>
      )
      break;

    default:
      return null
      break;
  }
}

const App = ({ progressIndex}) => {
  return (
    <div className="container pt-5 px-4">
      <div className="d-flex flex-column justify-content-start align-items-start mb-3">
        <small>booking storage at:</small>
        <h1 className="h4">Cody's Cookie Store</h1>
      </div>
      <Content progressIndex={progressIndex} />
    </div>
  )
}

const mapStateToProps = state => {
  return { progressIndex: state.progressIndex };
};

export default connect(mapStateToProps, {})(App)
