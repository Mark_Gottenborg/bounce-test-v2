import React from 'react'
import { connect } from 'react-redux'

function Loading () {
  return (
    <div className="modal w-100 h-100 d-flex justify-content-center align-items-center">
      <h2>LOADING . . .</h2>
    </div>
  );
}

export default connect(null, {})(Loading);