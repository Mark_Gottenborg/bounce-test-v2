import React from 'react';
import { connect } from 'react-redux'

import { increment, decrement } from '../../redux/actions'

const Product = ({count, increment, decrement}) => {
  return (
    <div className="d-flex flex-row justify-content-between align-items-center">
      <span className="text-nowrap">Number of bags:</span>
      <div className="d-flex flex-row justify-content-center align-items-center">
        <button onClick={decrement} disabled={count <= 1} className="btn btn-sm btn-primary">-</button>
        <span className="col-2">{count}</span>
        <button onClick={increment} disabled={count >= 2} className="btn btn-sm btn-primary">+</button>
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return { count: state.count };
};

export default connect(mapStateToProps, { increment, decrement })(Product)
