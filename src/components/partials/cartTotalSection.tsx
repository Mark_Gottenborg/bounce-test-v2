import React, {useState} from 'react'
import { connect } from 'react-redux'

import { incrementProgressIndex } from '../../redux/actions'


function CartTotalSection({ count, disableNextStep, progressIndex, incrementProgressIndex, error}) {
  return (
    <div className="container fixed-bottom d-flex flex-row justify-content-between align-items-center pb-3">
      <div className="d-flex flex-column justify-content-start align-items-center">
        <small>{count} bag(s)</small>
        <span>$ {count * 5.90}</span>
      </div>
      <button
        onClick={incrementProgressIndex}
        className="btn btn-primary"
        disabled={disableNextStep}
      >
        {progressIndex > 1 ? error ? 'Retry' : 'Book' : 'Next'}
      </button>
    </div>
  );
}

const mapStateToProps = state => {
  return { count: state.count, disableNextStep: state.disableNextStep, progressIndex: state.progressIndex, error: state.error };
};

export default connect(mapStateToProps, {incrementProgressIndex})(CartTotalSection);