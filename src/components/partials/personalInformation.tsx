import React from 'react'
import { connect } from 'react-redux'

import { changeName, changeEmail } from '../../redux/actions'

const PersonalInformation = ({name, email, changeName, changeEmail}) => {
  return (
    <div className="d-flex flex-column justify-content-start align-items-start my-3">
      <h2 className="h6">Personal Details:</h2>
      <div className="form-group">
        <label htmlFor="name">Name:</label>
        <input
          className="form-control"
          type="text"
          onChange={e => changeName(e.target.value)}
          name="name"
          id="name"
          value={name} />
      </div>
      <div className="form-group">
        <label htmlFor="email">Email:</label>
        <input
          id="email"
          className="form-control"
          type="email"
          value={email}
          onChange={e => changeEmail(e.target.value)}
          name="email" />
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return { name: state.name, email:state.email };
};

export default connect(mapStateToProps, { changeName, changeEmail })(PersonalInformation)


