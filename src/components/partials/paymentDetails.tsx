import React from 'react';
import { connect } from 'react-redux'

import { changeCardInfo } from '../../redux/actions'



const PaymentDetails = ({changeCardInfo}) => {
  return (
    <div className="d-flex flex-column justify-content-start align-items-start">
      <h2 className="h6">Payment information:</h2>
      <div className="form-group">
        <label htmlFor="cardDetails">card Details:</label>
        <input className="form-control" onChange={e => changeCardInfo(e.target.value)} max="9999999999999999" type="number" name="name" id="cardDetails"/>
      </div>
    </div>
  )
}

export default connect(null, {changeCardInfo})(PaymentDetails)