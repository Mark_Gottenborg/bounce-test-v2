import {
  INCREMENT,
  DECREMENT,
  CHANGENAME,
  CHANGEEMAIL,
  CHANGECARDINFO,
  INCREMENTPROGRESSINDEX } from './actionTypes'

const initialState = {
  count: 1,
  name: '',
  email: '',
  cardInfo: '',
  progressIndex: 0,
  loading: false,
  error: false,
  disableNextStep: false
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return {
        ...state,
        count: state.count + 1
      }
      break;

    case DECREMENT:
      return {
        ...state,
        count: state.count - 1
      }
      break;

    case CHANGENAME:
      let disableNextStep = true
      if(state.progressIndex === 1 && state.email.length > 0 && action.payload.length > 0) {
        disableNextStep
      }
      return {
        ...state,
        name: action.payload,
        disableNextStep: disableNextStep
      }
      break;

    case CHANGEEMAIL:
      disableNextStep = true
      if(state.progressIndex === 1 && action.payload.length > 0 && state.name.length > 0) {
        disableNextStep =  false
      }
      return {
        ...state,
        email: action.payload,
        disableNextStep: disableNextStep
      }
      break;

    case CHANGECARDINFO:
      disableNextStep = true
      if(state.progressIndex === 2 && action.payload.length === 16) {
        disableNextStep =  false
      }
      return {
        ...state,
        cardInfo: action.payload,
        disableNextStep: disableNextStep
      }
      break;

    case INCREMENTPROGRESSINDEX:
      return {
        ...state,
        progressIndex: state.progressIndex +1,
        disableNextStep: true
      }
      break;

    default:
      return state
      break;
  }
}

export default reducer