import {INCREMENT, DECREMENT, CHANGENAME, CHANGEEMAIL, CHANGECARDINFO, INCREMENTPROGRESSINDEX } from './actionTypes'


export const increment = content => ({
  type: INCREMENT,
})

export const decrement = content => ({
  type: DECREMENT,
})


export const changeName = content => ({
  type: CHANGENAME,
  payload: content
})

export const changeEmail = content => ({
  type: CHANGEEMAIL,
  payload: content
})

export const changeCardInfo = content => ({
  type: CHANGECARDINFO,
  payload: content
})

export const incrementProgressIndex = () => ({
  type: INCREMENTPROGRESSINDEX
})