export const VisibilityFilters = {
  SHOW_PRODUCTS: 'SHOW_PRODUCTS',
  SHOW_PERSONAL: 'SHOW_PERSONAL',
  SHOW_CARD_DETAILS: 'SHOW_CARD_DETAILS',
  SHOW_LOADING: 'SHOW_LOADING',
  SHOW_ORDER_COMPLETE: 'SHOW_ORDER_COMPLETE'
}

export const INCREMENT = 'INCREMENT'
export const DECREMENT = 'DECREMENT'
export const CHANGENAME = 'CHANGENAME'
export const CHANGEEMAIL = 'CHANGEEMAIL'
export const CHANGECARDINFO = 'CHANGECARDINFO'

export const INCREMENTPROGRESSINDEX = 'INCREMENTPROGRESSINDEX'